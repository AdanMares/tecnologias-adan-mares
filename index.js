const express = require('express')
const path = require('path')
const actions = require('./models/actions')

const equips = {}
let numEquips = 0
let maxTemperature = 27
let minTemperature = 25
let luz = false
let leds = false
let humidi = false

const app = express()

require('./database')

app.use(express.json())
app.set('port', process.env.PORT || 9000)

app.get('/', (req, res) =>{
    res.sendFile(path.join(__dirname+'/index.html'));
})

app.get('/esp', (req, res) => {
    const { nameESP } = req.body

    const response = {
        message: true
    }
    res.status(200).jsonp(response)
})

app.post('/request', (req, res) => {
    let equip
    let num = 0
    if (req.body.cooler && req.body.hot == 1) {
        for (let index = 1; index <= numEquips; index++) {
            if (equips['ESP' + index]) {
                equip = 'ESP' + index
                num++
            }
        }
        if (num == 1) {
            equips[equip].coolerStatus = true
            console.log(equips)
            res.status(200).jsonp({message: 'ok'})
        } else if (num > 1) {
            res.status(400).jsonp({message: 'tienes mas de 2 aires acondicionados por favor di programar'})
        } else if (num < 0) {
            res.status(400).jsonp({message: 'no tienes ningun aire acondicionado programado'})
        } else res.status(400).jsonp({message: 'error inesperado'})
    } else res.status(200).jsonp({message: 'ok'})
})

app.get('/init', async (req, res) =>{
    const {ip, sensorTH} = req.query
    console.log(req)
    numEquips++
    const Name = 'ESP' + numEquips
    const response = {
        message: true,
        Name,
        ip,

    }
    equips[Name] = {
        ip,
        sensorTH
    }
    const action = new actions({Type: "Conexion", description: "nuevo equipo conectado: " + Name})
    await action.save()
    res.status(200).jsonp(response)
})

app.get('/sedTemeparture', async (req, res) =>{
    const {Name, temperature, humedad} = req.query
    equips[Name].temperature = temperature
    const action = new actions({Type: "Humedad", description: "Humedad:  " + humedad})
    await action.save()
    res.status(200).jsonp({message: 'ok'})
})

app.get('/getLuz', async (req, res) =>{
    const {Name} = req.query
    equips[Name].luz  = true
    if (luz) {
        const action = new actions({Type: "Control de luz", description: 'leces encendidaz'})
        await action.save()
        res.status(200).jsonp({statusLuz: 'si'})
    }
    else {
        const action = new actions({Type: "Control de luz", description: 'luces apagadas'})
        await action.save()
        res.status(200).jsonp({statusLuz: 'no'})
    }
})

app.get('/getLeds', async (req, res) =>{
    const {Name} = req.query
    equips[Name].leds  = true
    if (leds) {
        const action = new actions({Type: "Control de leds", description: 'leds encendidos'})
        await action.save()
        res.status(200).jsonp({statusLeds: 'si'})
    }
    else {
        const action = new actions({Type: "Control de leds", description: 'leds apagados'})
        await action.save()
        res.status(200).jsonp({statusLeds: 'no'})
    }
})

app.get('/getHumidi', async (req, res) =>{
    const {Name} = req.query
    equips[Name].humidi  = true
    if (humidi) {
        const action = new actions({Type: "Control de humedad", description: 'humidificador encendido'})
        await action.save()
        res.status(200).jsonp({statusHumidi: 'si'})
    }
    else {
        const action = new actions({Type: "Control de humedad", description: 'humidificador apagado'})
        await action.save()
        res.status(200).jsonp({statusHumidi: 'no'})
    }
})

app.post('/setHumidi', async (req, res) =>{
    const {setHumidi} = req.body
    humidi = setHumidi
    console.log(humidi)
    const action = new actions({Type: "Control de humedad", description: "se puso el control de humedad en modo: " + setHumidi})
    await action.save()
    res.status(200).jsonp({message: humidi})
})

app.post('/setLeds', async (req, res) =>{
    const {setLeds} = req.body
    leds = setLeds
    console.log(leds)
    const action = new actions({Type: "Control de leds", description: "se puso el control de leds en modo: " + setLeds})
    await action.save()
    res.status(200).jsonp({message: leds})
})

app.post('/setLuz', async (req, res) =>{
    const {setLuz} = req.body
    luz = setLuz
    console.log(luz)
    const action = new actions({Type: "Control de luz", description: "se puso el control de luz en modo: " + setLuz})
    await action.save()
    res.status(200).jsonp({message: luz})
})

app.post('/setTempMin', async (req, res) =>{
    const {minTemperatures} = req.body
    minTemperature = minTemperatures
    const action = new actions({Type: "Control temperatura", description: "se cambio la temperatura minima a: " + minTemperature})
    await action.save()
    res.status(200).jsonp({message: minTemperature})
})

app.post('/setTempMax', async (req, res) =>{
    const {maxTemperatures} = req.body
    maxTemperature = maxTemperatures
    const action = new actions({Type: "Control temperatura", description: "se cambio la temperatura maxima a: " + maxTemperature})
    await action.save()
    res.status(200).jsonp({message: maxTemperature})
})

app.get('/onCooler', async (req, res) =>{
    let temperature = 0
    let cant = 0;
    let status
    for (let index = 0; index < Object.keys(equips).length; index++) {
        const element = Object.keys(equips)[index];
        if (equips[element].temperature) {
            cant++;
            temperature += parseFloat(equips[element].temperature);
            console.log(equips[element])
        }
        
    }
    temperature = temperature / cant
    const action = new actions({Type: "Control temperatura", description: "temperatura: " + temperature + ' °C'})
    await action.save()
    if (temperature > maxTemperature) {
        status = 'cool'
    } else if (temperature < minTemperature) {
        status = 'hot'
    } else status = 'none'
    res.status(200).jsonp({status})
})

app.post('/status', (req, res) =>{
    const {nameESP} = req.body
    res.status(200).jsonp(equips[nameESP])
})

app.get('/equips', (req, res) =>{
    res.status(200).json(equips)
})

app.get('/getTemperatureMin', (req, res) =>{
    res.status(200).json({temperatureMin: minTemperature})
})

app.get('/getTemperatureMax', (req, res) =>{
    res.status(200).json({temperatureMax: maxTemperature})
})

app.get('/getLuzs', (req, res) =>{
    res.status(200).json({luz: luz})
})
app.get('/getLedss', (req, res) =>{
    res.status(200).json({leds: leds})
})
app.get('/getHumidity', (req, res) =>{
    res.status(200).json({Humidificador: humidi})
})

app.get('/all', async (req, res) =>{
    const response = await actions.find()
    res.status(200).json(response)
})

app.get('/dbLuz', async (req, res) =>{
    const response = await actions.find({Type: "Control de luz"})
    res.status(200).json(response)
})

app.get('/dbHumedad', async (req, res) =>{
    const response = await actions.find({Type: ""})
    res.status(200).json(response)
})

app.get('/dbLeds', async (req, res) =>{
    const response = await actions.find({Type: "Control de leds"})
    res.status(200).json(response)
})

app.get('/dbHumidi', async (req, res) =>{
    const response = await actions.find({Type: "Humedad"})
    res.status(200).json(response)
})

app.get('/dbTemp', async (req, res) =>{
    const response = await actions.find({Type: "Control temperatura"})
    res.status(200).json(response)
})

app.get('/equipos', async (req, res) =>{
    const response = await actions.find({Type: "Conexion"})
    res.status(200).json(response)
})

app.get('/dbInit', async (req, res) =>{
    const response = await actions.find({Type: "Encendido del sistema"})
    res.status(200).json(response)
})

app.post('/getConsumo', async (req, res) =>{
    const response = await actions.find()
    res.status(200).json({Consumo: '1500W'})
})

app.post('/getTemperatura', async (req, res) =>{
    const response = await actions.find({Type: "Control temperatura"})
    res.status(200).json({temperatura: '24°C'})
})

app.post('/getHumedad', async (req, res) =>{
    const response = await actions.find({Type: "Humedad"})
    res.status(200).json({humedad: '30%'})
})

app.listen(app.get('port'), async ()=> {
    const action = new actions({Type: "Encendido del sistema", description: "se inicio el sistema"})
    await action.save()
    console.log('Server on port ', app.get('port'))
})