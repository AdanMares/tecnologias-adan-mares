/*const axios = require('axios')
const api = 'AIzaSyDcrX3fYWEJxoRFXdk8xe4x9ikDzku1idc'
const request = {

}*/
/*const speech = require('@google-cloud/speech')
const AudioRecorder = require('node-audiorecorder')
const fs = require('fs')

function init() {
    const options = {
        program: `rec`,
        device: null,

        bits: 16,
        channels: 1,
        encoding: `signed-integer`, 
        format: `S16_LE`,
        rate: 16000,
        type: `wav`,        // Format type.

        silence: 2,
        thresholdStart: 0.5,
        thresholdStop: 0.5,
        keepSilence: true
    }
    let audioRecorder = new AudioRecorder(options, console)
    audioRecorder.start()
    setTimeout(()=>{
        audioRecorder.stream() 
        audioRecorder.stop()
        console.log(audioRecorder)
    }, 3000)
}

async function speechFunction(audioBytes) {
    const client = new speech.SpeechClient()
    const audio = {
        content: audioBytes
    }
    const config = {
        encoding: 'LINEAR16',
        sampleRateHeartz: 16000,
        languageCode: 'es-ES'
    }
    const request = {
        audio,
        config
    }

    const [response] = await client.recognize(request)
    console.log(response)
}

init()*/
const express = require('express')
const https = require('https')
const path = require('path')
const fs = require('fs')

const app = express()
app.use(express.json())
app.set('port', process.env.PORT || 9002 )

app.get('/', (req, res) =>{
    res.sendFile(path.join(__dirname+'/index.html'));
})

/*const sslserver = https.createServer({
    key: '',
    cert: ''
}, app)*/

app.listen(app.get('port'), ()=> {
    console.log('Server on port ', app.get('port'))
})