const mongoose = require('mongoose')
const { Schema } = mongoose

const actionSchema = new Schema({
    Type: { 
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('action', actionSchema)